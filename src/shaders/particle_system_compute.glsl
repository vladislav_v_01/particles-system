#version 430

layout( local_size_x = 1000) in;

uniform float Gravity1 = 2000.0f;
uniform vec3 BlackHolePos1 = vec3(5.0f, 0.0f, 0.0f);
uniform float Gravity2 = 2000.0f;
uniform vec3 BlackHolePos2 = vec3(-5.0f, 0.0f, 0.0f);
uniform float ParticleInvMass = 1.0f / 0.1f;
uniform float DeltaT = 0.0005f;
uniform float maxDist = 45.0f;

layout(std430, binding=0) buffer Pos {
  vec4 Position[];
};

layout(std430, binding=1) buffer Vel {
  vec4 Velocity[];
};

void main() {
  uint idx = gl_GlobalInvocationID.x;
  vec3 p = Position[idx].xyz;
  vec3 v = Velocity[idx].xyz;
  // Сила воздействия черной дыры #1
  vec3 d = BlackHolePos1 - p;
  float dist = length(d);
  vec3 force = (Gravity1 / dist) * normalize(d);
  // Сила воздействия черной дыры #2
  d = BlackHolePos2 - p;
  force += (Gravity2 / dist) * normalize(d);
  // Интегрирование простым методом Эйлера
  if ( dist > maxDist ){
     Position[idx] = vec4(0,0,0,1);
  }
  else{
  vec3 a = force * ParticleInvMass;
  Position[idx] = vec4(
      p + v * DeltaT + 0.5 * a * DeltaT * DeltaT, 1.0);
  Velocity[idx] = vec4( v + a * DeltaT, 0.0);
  }
}

#version 430

layout(location = 0) in vec3 aPos;

out vec3 outColor;

void main(){
    gl_Position = vec4(aPos, 1.0);
    outColor = vec3(aPos.xyz);
}
